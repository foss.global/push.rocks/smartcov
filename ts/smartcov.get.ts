import plugins = require('./smartcov.plugins')

export let percentageFromLcovString = (fileStringArg: string, roundedTo: number, format?: string): Promise<number> => {
  let done = plugins.q.defer<number>()
  let report = fileStringArg
  if (report === '') {
    console.log('can\'t parse empty string')
    done.reject('can\'t parse empty string')
    return
  }
  let coveragePercentage: number
  let finalCoverage: number
  format = format || 'lcov'
  switch (format) {
    case 'jscoverage':
      plugins.beautylog.warn('jscoverage not yet implemented')
      done.reject('error')
      break
    case 'lcov':
    default:
      plugins.lcovParse(report, (err, data) => {
        if (err) {
          console.log(err)
        };
        let hit = 0
        let found = 0
        for (let i = 0; i < data.length; i++) {
          hit += data[ i ].lines.hit
          found += data[ i ].lines.found
        }
        coveragePercentage = (hit / found) * 100
        finalCoverage = plugins.mathjs.round(coveragePercentage, roundedTo)
        done.resolve(finalCoverage)
      })
      break
  }
  return done.promise
}

export let percentageFromLcovFile = async (filePathArg: string, roundedToArg: number, formatArg?: string): Promise<number> => {
  let report: string
  try {
    report = plugins.fs.readFileSync(filePathArg, 'utf8')
  } catch (err) {
    plugins.beautylog.warn('no file found at ' + filePathArg)
    throw new Error('no file found at ' + filePathArg)
  }
  return await percentageFromLcovString(report, roundedToArg, formatArg)
}
