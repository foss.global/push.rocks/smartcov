"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("typings-global");
const beautylog = require("beautylog");
exports.beautylog = beautylog;
let fs = require('fs-extra');
exports.fs = fs;
let lcovParse = require('lcov-parse');
exports.lcovParse = lcovParse;
let mathjs = require('mathjs');
exports.mathjs = mathjs;
const q = require("smartq");
exports.q = q;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRjb3YucGx1Z2lucy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3RzL3NtYXJ0Y292LnBsdWdpbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwwQkFBdUI7QUFDdkIsdUNBQXVDO0FBT3JDLDhCQUFTO0FBTlgsSUFBSSxFQUFFLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBTzFCLGdCQUFFO0FBTkosSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBT25DLDhCQUFTO0FBTlgsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBTzVCLHdCQUFNO0FBTlIsNEJBQTRCO0FBTzFCLGNBQUMifQ==